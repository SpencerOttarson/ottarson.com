terraform {
  backend "s3" {
    # Replace this with your bucket name!
    bucket         = "spencerottarson-universal-terraform-state"
    key            = "global/s3/terraform.tfstate"
    region         = "us-east-2"
  }
}