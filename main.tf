provider "aws" {
  region = "us-east-2"
}

resource "aws_route53_zone" "primary" {
  name = "${var.domain}"
}

resource "aws_route53_record" "a" {
  zone_id = "${aws_route53_zone.primary.zone_id}"
  name    = "${var.domain}"
  type    = "A"
  ttl     = "300"
  records = ["0.0.0.0"]
}

resource "aws_route53_record" "mx" {
  zone_id = "${aws_route53_zone.primary.zone_id}"
  name    = "${var.domain}"
  type    = "MX"
  ttl     = "300"
  records = ["10 mail.protonmail.ch", "20 mailsec.protonmail.ch"]
}

resource "aws_route53_record" "nameservers" {
  allow_overwrite = true
  zone_id         = "${aws_route53_zone.primary.zone_id}"
  name            = "${var.domain}"
  ttl             = 86400
  type            = "NS"

  records = [
    "${aws_route53_zone.primary.name_servers.0}.",
    "${aws_route53_zone.primary.name_servers.1}.",
    "${aws_route53_zone.primary.name_servers.2}.",
    "${aws_route53_zone.primary.name_servers.3}."
  ]
}

resource "aws_route53_record" "txt" {
  zone_id = "${aws_route53_zone.primary.zone_id}"
  name    = "${var.domain}"
  type    = "TXT"
  ttl     = "300"
  records = ["protonmail-verification=${var.verification_token}", "v=spf1 include:_spf.protonmail.ch mx ~all"]
}

resource "aws_route53_record" "domainkey" {
  zone_id = "${aws_route53_zone.primary.zone_id}"
  name    = "protonmail._domainkey.${var.domain}"
  type    = "TXT"
  ttl     = "300"
  records = ["v=DKIM1; k=rsa; p=${var.domain_key_password}"]
}
